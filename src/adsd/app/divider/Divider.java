/**
 * Class Divider.java
 *
 * Demonstrates divider functionality non defensive
 */
package adsd.app.divider;

import java.util.Scanner;

public class Divider
{
   /**
    * Calculates the quotient of the given number and denominator
    *
    * @param  numerator number as integer
    * @param  denominator denominator as integer
    * @return Quotient as result of a division
    */   
   public static int quotient( int numerator, int denominator )
   {
      return numerator / denominator; // possible division by zero
   } // end method quotient


   /**
    * Main method of this class
    *
    * @param args[] program arguments list as String array
    */
   public static void main( String args[] )
   {
      // Get user input
      Scanner scanner = new Scanner( System.in );
 
      // Display results
      System.out.print( "Please enter an integer numerator: " );
      int numerator = scanner.nextInt();
      System.out.print( "Please enter an integer denominator: " );
      int denominator = scanner.nextInt();

      int result = quotient( numerator, denominator );
      System.out.printf( "\nResult: %d / %d = %d\n", numerator,
                           denominator, result );
 
   }

}
